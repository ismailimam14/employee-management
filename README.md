### Quick Start

#### 1. Clone repository

```
git clone https://gitlab.com/ismailimam14/employee-management.git
```

#### 2. Pindah ke direktori employee-management

```
cd employee-management
```

#### 3. Install Semua Dependencies

```
npm install
```

#### 4. Jalankan project

Development:

```
ng serve -o
```

Production:

```
ng serve --configuration production -o
```