import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { NotFoundComponent } from './components/not-found/not-found.component';

const routes: Routes = [
  {path: "login", component : LoginComponent},
  {
    path: 'employee', 
    loadChildren: ()=>import('./modules/employee/employee.module')
    .then((m)=>m.EmployeeModule)},
  {path: '', redirectTo : '/login', pathMatch : "full"},
  {path : '**', component : NotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash : true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
