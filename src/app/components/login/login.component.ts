import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { timeout } from 'rxjs/operators';
// import { faLock } from '@fortawesome/free-solid-svg-icons';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  // falock = faLock;
  loginForm = new FormGroup({
    username : new FormControl('', [Validators.required]),
    password : new FormControl('', [Validators.required]),
  })

  isError : boolean = false;
  msgError! : string;
  constructor(private authService : AuthService, private router : Router) { }

  ngOnInit(): void {
    if(this.authService.isLoggedIn()){
      this.router.navigate(['employee']);
    }
  }

  onSubmit() : void{
    if(this.loginForm.valid){
      console.log(this.loginForm.value);
      this.authService.login(this.loginForm.value).subscribe((result)=>{
        console.log('result login', result);
        this.router.navigate(['employee']);
      }, (err : Error)=>{
         
        this.msgError = err.message;
        this.isError = true;

        setTimeout(() => {
          this.isError = false
        }, 5000);
        
      })
    }
  }

  get userName() : FormControl{
    return this.loginForm.get('username') as FormControl;
  }

  get password() : FormControl{
    return this.loginForm.get('password') as FormControl;
  }

  validate(){
    return (this.userName.touched && this.userName.invalid) || (this.password.touched && this.password.invalid) ? 
    'was-validated' : ''
  }

  getControl(control : string) : FormControl{
    return this.loginForm.get(control) as FormControl;
  }

  close(){
    this.isError = false;
  }

}
