import { CurrencyPipe, DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators'
import { EmployeeService } from 'src/app/services/employee.service';
import { ErrorDialog } from '../error-dialog.component';
import { SuccessDialog } from '../success-dialog.';

@Component({
  selector: 'app-employee-add',
  templateUrl: './employee-add.component.html',
  styleUrls: ['./employee-add.component.scss']
})
export class EmployeeAddComponent implements OnInit {

  employeForm! : FormGroup;
  groupControl = new FormControl('');
  options: string[] = ['Bubblemix', 'Midel', 'Twitterbeat', 'Feedbug', 'Edgewire', 'Twinder', 'Snaptags','Voonder', 'Jabbertype', 'Voonix'];
  filteredOptions! : Observable<string[]>;

  maxDate! : Date;
  minDate! : Date
  
  constructor(
    private fb : FormBuilder, 
    public router : Router,
    private employeeService : EmployeeService,
    public dialog: MatDialog,
    private datePipe : DatePipe
    ) { }

  ngOnInit(): void {
    this.initializeForm();
    this.filteredOptions = this.groupControl.valueChanges
      .pipe( 
        startWith(''),
        map(value => this._filter(value))
      );

    const currentYear = new Date().getFullYear();
    this.minDate = new Date(currentYear - 70, 0, 1);
    this.maxDate = new Date();
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.options.filter(option => option.toLowerCase().includes(filterValue));
  }

  initializeForm(){
    this.employeForm = this.fb.group({
      username : [null, Validators.required],
      firstName : [null, [Validators.required, this.noSpaceAllowed, Validators.pattern("[a-zA-Z]+$")]],
      lastName : [null, [Validators.required, this.noSpaceAllowed, Validators.pattern("[a-zA-Z]+$")]],
      email : [null, [Validators.required, Validators.email, this.noSpaceAllowed]],
      birthDate : [null, Validators.required],
      basicSalary : [null, [Validators.required, Validators.pattern("[0-9]+$")]],
      status : [null, Validators.required],
      group : this.groupControl,
      description : [null, [Validators.required]]
    })
  }

  get basicSalary() : FormControl{ 
    return this.employeForm.get('basicSalary') as FormControl;
  }

  onSubmit(){
    if(this.employeForm.valid){
      this.birthDate.setValue(this.datePipe.transform(this.birthDate.value, 'dd/MM/yyyy'))
      this.employeeService.addUserData(this.employeForm.value);
      this.router.navigate(['/']);
      this.dialog.open(SuccessDialog);
    } else {
      this.dialog.open(ErrorDialog);
    }
    
    console.log(this.employeForm.value); 
  }

  noSpaceAllowed(control : FormControl){
    if(control.value && control.value.indexOf(" ") > -1){
      return {noSpaceAllowed : true};
    }
    return null;
  }

  getControl(controlName : string) : FormControl{
    return this.employeForm.get(controlName) as FormControl;
  }

  get birthDate(){
    return this.getControl('birthDate');
  }
  
  validateClass(controlName : string, type : string){
    if(type=="border"){
      this.getControl(controlName).touched && this.getControl(controlName).invalid ?'border border-danger' : '';
    }
  }

  backToHome(){
    this.router.navigate(['/'])
  }
}


