import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EmployeeService } from 'src/app/services/employee.service';
import { Employee } from '../employee.model';

@Component({
  selector: 'app-employee-detail',
  templateUrl: './employee-detail.component.html',
  styleUrls: ['./employee-detail.component.scss']
})
export class EmployeeDetailComponent implements OnInit {

  data! : Employee;
  constructor(
    private route : ActivatedRoute,
    private employeeService : EmployeeService,
    public router : Router
    ) { }

  ngOnInit(): void {
    this.route.params.subscribe(params=>{
      console.log('params', params.username);

      const data = this.employeeService.getData();

      if(data){
        data.map(item=>{
          if(item.username==params.username){

            if(typeof item.birthDate=="string"){
              var s : any = item.birthDate.split("/");  //d/m/yyyy
              var d = new Date(
                Number(s[2]),
                Number(s[1]),
                Number(s[0]));
              item.birthDate = d;
            }
          
            this.data = item;
          }
        })
      }
      
    })
  }

  edit(uname : string){
    this.router.navigate(['employee/edit/',uname])
  }

  back(){
    this.router.navigate(['/'])
  }

}
