import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { startWith, map, switchMap } from 'rxjs/operators'
import { EmployeeService } from 'src/app/services/employee.service';
import { ErrorDialog } from '../error-dialog.component';
import { SuccessDialog } from '../success-dialog.';

@Component({
  selector: 'app-employee-edit',
  templateUrl: './employee-edit.component.html',
  styleUrls: ['./employee-edit.component.scss']
})
export class EmployeeEditComponent implements OnInit {

  employeForm! : FormGroup;
  groupControl = new FormControl();
  options: string[] = ['Google', 'Facebook', 'Tesla'];
  filteredOptions! : Observable<string[]>;

  maxDate! : Date;
  minDate! : Date
  
  constructor(
    private fb : FormBuilder, 
    public router : Router,
    public route : ActivatedRoute,
    private employeeService : EmployeeService,
    private dialog : MatDialog
    ) { }

  ngOnInit(): void {
    this.initializeForm();
    this.filteredOptions = this.groupControl.valueChanges
      .pipe( 
        startWith(''),
        map(value => this._filter(value))
      );

      this.route.params.subscribe(params=>{
        console.log('params', params.username);

        const data = this.employeeService.getData();

        if(data){
          data.map(item=>{
            if(item.username==params.username){

              if(typeof item.birthDate=="string"){
                var s : any = item.birthDate.split("/");  //d/m/yyyy
                var d = new Date(
                  Number(s[2]),
                  Number(s[1]),
                  Number(s[0]));
                item.birthDate = d;
              }
            
              this.employeForm.setValue(item);
            }
          })
        }
        
      })
      

      // this.getControl('basicSalary')?.valueChanges.subscribe(value=>{
      // })
       
      this.getControl('birthDate')?.valueChanges.subscribe(value=>{
        console.log('birthdate',value)
        value
      });

    const currentYear = new Date().getFullYear();
    this.minDate = new Date(currentYear - 70, 0, 1);
    this.maxDate = new Date();
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.options.filter(option => option.toLowerCase().includes(filterValue));
  }

  initializeForm(){
    this.employeForm = this.fb.group({
      username : [null, Validators.required],
      firstName : [null, [Validators.required, this.noSpaceAllowed, Validators.pattern("[a-zA-Z]+$")]],
      lastName : [null, [Validators.required, this.noSpaceAllowed, Validators.pattern("[a-zA-Z]+$")]],
      email : [null, [Validators.required, Validators.email, this.noSpaceAllowed]],
      birthDate : [null, Validators.required],
      basicSalary : [null, Validators.required],
      status : [null, Validators.required],
      group : this.groupControl,
      description : [null, [Validators.required]]
    })
  }

  get basicSalary() : FormControl{ 
    return this.employeForm.get('basicSalary') as FormControl;
  }

  onSubmit(){
    console.log(this.employeForm.value); 
    if(this.employeForm.valid){
      this.employeeService.editUserData(this.employeForm.value);
      this.router.navigate(['/']);
      this.dialog.open(SuccessDialog);
    } else {
      this.dialog.open(ErrorDialog);
    }
  }

  noSpaceAllowed(control : FormControl){
    if(control.value && control.value.indexOf(" ") > -1){
      return {noSpaceAllowed : true};
    }
    return null;
  }

  getControl(controlName : string) : FormControl{
    return this.employeForm.get(controlName) as FormControl;
  }

  get birthDate(){
    return this.getControl('birthDate');
  }
  
  validateClass(controlName : string, type : string){
    if(type=="border"){
      this.getControl(controlName).touched && this.getControl(controlName).invalid ?'border border-danger' : '';
    }
  }

  backToHome(){
    this.router.navigate(['/'])
  }
}

