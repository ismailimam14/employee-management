import { LiveAnnouncer } from '@angular/cdk/a11y';
import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from "@angular/material/table"
import { Router } from '@angular/router';
import { EmployeeService } from 'src/app/services/employee.service';
import { Employee } from '../employee.model';
import { ErrorDialog } from '../error-dialog.component';
import { SuccessDialog } from '../success-dialog.';



@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.scss']
})
export class EmployeeListComponent implements OnInit {

  displayedColumns = ["firstName", "email", "birthDate", "group", "action"];
  dataSource! : MatTableDataSource<Employee>;

  @ViewChild(MatPaginator) paginator! : MatPaginator;
  @ViewChild(MatSort) matSort! : MatSort;

  isSuccess : boolean = false;
  isError : boolean = false;
  desserts : any = [];
  constructor(
    public employeService : EmployeeService,
    private matDialog : MatDialog,
    public router : Router,
    private _liveAnnouncer: LiveAnnouncer
    ) { }

  ngOnInit(): void {
    const res = this.employeService.getData();
    if(res){
      this.dataSource = new MatTableDataSource(res);
    }

    if(this.employeService.tmpSearch !== ""){
      console.log('e')
      this.dataSource.filter = this.employeService.tmpSearch.trim().toLowerCase();
    }
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.matSort;
  }

  sortChange(sortState : Sort){
    const data = this.dataSource.data.slice();
    if (!sortState.active || sortState.direction === '') {
      this.dataSource.data = data;
      return;
    }

    this.dataSource.data = data.sort((a, b) => {
      const isAsc = sortState.direction === 'asc';
      switch (sortState.active) {
        case 'firstName':
          return compare(a.firstName, b.firstName, isAsc);
        case 'email':
          return compare(a.email, b.email, isAsc);
        case 'basicSalary':
          return compare(a.basicSalary, b.basicSalary, isAsc);
        case 'birthDate':
          return compareDate(a.birthDate, b.birthDate, isAsc);
        case 'status':
          return compare(a.status, b.status, isAsc);
        case 'group':
          return compare(a.group, b.group, isAsc);
        case 'description':
          return compare(a.description, b.description, isAsc);
        default:
          return 0;
      }
    });

    if (sortState.direction) {
      this._liveAnnouncer.announce(`Sorted ${sortState.direction} ending`);
    } else {
      this._liveAnnouncer.announce('Sorting cleared');
    }

    function compare(a: number | string, b: number | string, isAsc: boolean) {
      return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
    }

    function compareDate(a : string | Date, b : string | Date, isAsc: boolean){
      if(typeof a==="string" && a.includes("/") && typeof b==="string" && b.includes("/")){
        var sa = a.split("/");
        var sb = b.split("/");
        var aa = new Date(
          Number(sa[2]), 
          Number(sa[1]), 
          Number(sa[0]))

          var bb = new Date(
            Number(sb[2]), 
            Number(sb[1]), 
            Number(sb[0]))

        return (aa < bb ? -1 : 1) * (isAsc ? 1 : -1);
      } else {
        return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
      }
    }
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    this.employeService.tmpSearch=filterValue;
  }

  askDelete(uname : string){
    const dialogRef = this.matDialog.open(ConfirmDeleteDialog);  
    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.employeService.deleteUserData(uname, (res : any)=>{
          if(res){
            this.router.navigate(['/'])
            this.matDialog.open(SuccessDialog);
          } else {
            this.router.navigate(['/'])
            this.matDialog.open(ErrorDialog);
          }
        })
      }
    });
  }

}

@Component({
  selector: 'dialog-confirm',
  template : `
  <h1 mat-dialog-title>Delete Employee Data</h1>
  <div mat-dialog-content>
    Are you sure delete this data?
  </div>
  <div mat-dialog-actions>
    <button mat-button mat-dialog-close>cancel</button>
    <button mat-button cdkFocusInitial (click)="sure()">Sure</button>
  </div>`
})
export class ConfirmDeleteDialog {
  constructor(public dialogRef: MatDialogRef<EmployeeListComponent>) {}

  sure(){
    this.dialogRef.close(true)
  }
}
