import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from 'src/app/components/dashboard/dashboard.component';
import { EmployeeAddComponent } from './employee-add/employee-add.component';
import { EmployeeDetailComponent } from './employee-detail/employee-detail.component';
import { EmployeeEditComponent } from './employee-edit/employee-edit.component';
import { EmployeeListComponent } from './employee-list/employee-list.component';

const routes: Routes = [
  {path: "", component : DashboardComponent, children : [
    {path: 'add', component : EmployeeAddComponent},
    {path: 'edit/:username', component : EmployeeEditComponent},
    {path: 'detail/:username', component : EmployeeDetailComponent},
    {path: 'list', component : EmployeeListComponent},
    {path: "", redirectTo : '/employee/list', pathMatch : 'full'},
    {path: "**", redirectTo : '/employee/list', pathMatch : 'full'}
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployeeRoutingModule { }
