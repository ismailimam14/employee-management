import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from 'src/app/material.module';

import { EmployeeRoutingModule } from './employee-routing.module';
import { ConfirmDeleteDialog, EmployeeListComponent } from './employee-list/employee-list.component';
import { EmployeeAddComponent } from './employee-add/employee-add.component';
import { EmployeeEditComponent } from './employee-edit/employee-edit.component';
import { EmployeeDetailComponent } from './employee-detail/employee-detail.component';
import { ErrorMessage } from './error-message.component';
import { ErrorDialog } from './error-dialog.component';
import { SuccessDialog } from './success-dialog.';


@NgModule({
  declarations: [
    EmployeeListComponent,
    EmployeeAddComponent,
    EmployeeEditComponent,
    EmployeeDetailComponent,
    ErrorMessage,
    ConfirmDeleteDialog,
    ErrorDialog,
    SuccessDialog
  ],
  imports: [
    CommonModule,
    EmployeeRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
  ]
})
export class EmployeeModule { }
