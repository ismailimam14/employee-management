import { Component } from "@angular/core";

@Component({
    selector: 'error-dialog',
    template: `
    <h1 mat-dialog-title class="bg-error">Error</h1>
    <div mat-dialog-content>
        please fill out all required fields
    </div>
    <div mat-dialog-actions>
      <button mat-button mat-dialog-close>Close</button>
    </div>`,
  })
  export class ErrorDialog {}