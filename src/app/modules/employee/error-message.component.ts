import { Component, Input } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'error-message',
  template: `
  <div *ngIf="control.invalid && control.touched">
    <small *ngIf="control?.errors?.required" class="text-danger">
      The field is required
    </small>

    <small *ngIf="control?.errors?.noSpaceAllowed" class="text-danger">
      Content shoudln't contain spaces
    </small> 

    <small *ngIf="control?.errors?.email" class="text-danger">
      The email field must be a valid email
    </small> 

    <small *ngIf="control?.errors?.pattern" class="text-danger">
      The field is invalid format
    </small>
  </div>
   `,
   styles: ["small { display : block}"]
})

export class ErrorMessage {
  @Input() control! : FormControl;
  
  constructor() { }

}