import { Component } from "@angular/core";

@Component({
    selector: 'success-dialog',
    template: `
    <h1 mat-dialog-title class="bg-error">Sukses</h1>
    <div mat-dialog-content>
        Success
    </div>
    <div mat-dialog-actions>
      <button mat-button mat-dialog-close>Close</button>
    </div>`,
  })
  export class SuccessDialog {

    constructor(){
    }
  }