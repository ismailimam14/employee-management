import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { Employee } from '../modules/employee/employee.model';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  EMPLOYEE_DATA! : Employee[];
  key : string = "employeKeyv1";

  tmpSearch : string="";
  
  constructor() {
    this.loadData();
  }

  async loadData(){
    const data = await fetch('../../assets/employeeData.json');
    this.EMPLOYEE_DATA = await data.json();

    if(this.getData()==null){
      localStorage.setItem(this.key, JSON.stringify(this.EMPLOYEE_DATA));
    } else {
      this.getData();
    }

  }

  getData() : Employee[] | null{
    const data= localStorage.getItem(this.key);
    if(data){
      return JSON.parse(data);
    }
    return null;
  }

  addUserData(data : Employee){
    this.EMPLOYEE_DATA.unshift(data);
    localStorage.setItem(this.key, JSON.stringify(this.EMPLOYEE_DATA));
  }

  editUserData(data : Employee){
    var idx = this.EMPLOYEE_DATA.map(item=>item.username).indexOf(data.username);
    if(idx > -1){
      this.EMPLOYEE_DATA[idx] = data;
    } else {
      this.EMPLOYEE_DATA.unshift(data); // if username changed, then add new data
    }
    
    localStorage.setItem(this.key, JSON.stringify(this.EMPLOYEE_DATA));
  }

  deleteUserData(uname : string, callback : any){
    console.log('del')
    if(this.EMPLOYEE_DATA.length < 1) callback();
    
    console.log(this.EMPLOYEE_DATA.length)
    this.EMPLOYEE_DATA.forEach((item, i, arr)=>{
      if(item.username==uname){
        arr.splice(i, 1);
        
        this.EMPLOYEE_DATA = arr;
        localStorage.setItem(this.key, JSON.stringify(this.EMPLOYEE_DATA));
        callback({
          success : true,
          message : 'Success delete data'
        })
      }
    })
  }
}
